import os
import shutil
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import glob
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import keras
import subprocess
import cv2
import argparse
import logging
logger = logging.getLogger("process_videos")
from tempfile import gettempdir

from Mask_RCNN import default_model, default_class_names, visualize, visualize_mod

import camangle_calibration


def process_video(vid,arucopath,outputpath=".",fps=30,recalculate = False,MAKEVIDEOS = False,NOCLEANUP = False,numframes = None):
    basename = os.path.splitext(os.path.basename(vid))[0]
    if NOCLEANUP:
        framepath = os.path.join(outputpath,"temp_frames_" + basename)
        outframepath = os.path.join(outputpath,"temp_outframes_" + basename)
    else:
        framepath = os.path.join(gettempdir(), 'temp_frames_{}_{}'.format(basename,hash(os.times())))
        outframepath = os.path.join(gettempdir(), 'temp_outframes_{}_{}'.format(basename,hash(os.times())))
    videodirpath = os.path.join(outputpath, "outputvideos")

    logpath = os.path.join(outputpath,basename + "_raw_log.csv")
    if os.path.exists(logpath) and not recalculate:
        logger.info("log already exists, skipping")
        return None

    if os.path.isdir(framepath):
        shutil.rmtree(framepath)
    os.mkdir(framepath)

    if MAKEVIDEOS:
        logger.info("creating outputvideo frames in {}".format(outframepath))
        if os.path.isdir(outframepath):
            shutil.rmtree(outframepath)
        os.mkdir(outframepath)
    if numframes is None:
        subprocess.call('ffmpeg  -i "{}" -r {}  -f image2 "{}"'.format(vid,fps,os.path.join(framepath,'%5d.png')),shell=True)
    else:
        subprocess.call('ffmpeg  -i "{}" -r {} -vframes {} -f image2 "{}"'.format(vid,fps,numframes,os.path.join(framepath,'%5d.png')),shell=True)

    totalframes = len(glob.glob(os.path.join(framepath, "*.png")))
    with open(logpath, 'w') as log:
        view,val = camangle_calibration.recognize_view(os.path.join(framepath,'{:05d}.png'.format(1)),arucopath)
        log.write("# {} {} {}\n".format(view,val,arucopath))
        for i in range(1,len(os.listdir(framepath)) + 1):
            image = skimage.io.imread(os.path.join(framepath, "{:05d}.png".format(i)))
            print('Processing frame ' + str(i) + ' of ' + str(totalframes))

            # Run detection
            results = default_model.detect([image], verbose=1)

            # Visualize results
            r = results[0]
            for integer, index in enumerate(r['class_ids']):
            # We add all classes for the time being
                if True:
                    to_print = [i, (r['rois'][integer][0] + r['rois'][integer][2])//2, (r['rois'][integer][1] + r['rois'][integer][3])//2, r['rois'][integer][0], r['rois'][integer][2], r['rois'][integer][1], r['rois'][integer][3], default_class_names[index]]
                    print(','.join(map(str, to_print)) + '\n')
                    log.write(','.join(map(str, to_print)) + '\n')
            if MAKEVIDEOS:
                visualize_mod.display_instances(image, r['rois'], r['masks'], r['class_ids'], default_class_names, i, outframepath, r['scores'])

    if MAKEVIDEOS:
        if not os.path.isdir(videodirpath):
            os.mkdir(videodirpath)
        subprocess.call('ffmpeg  -y  -framerate {}  -i "{}" "{}"'.format(fps,os.path.join(outframepath,'%1d.png'),os.path.join(videodirpath,basename+"_masked.mp4")),shell=True)
        if not NOCLEANUP:
            shutil.rmtree(outframepath)
    if not NOCLEANUP:
        shutil.rmtree(framepath)
    return None

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process the videos.")
    parser.add_argument("-d", "--directory",
                help="directory with input videos", metavar="DIR",
                type=str,required = True)
    parser.add_argument("-o", "--outputdir",
                help="output directory (default is input dir)", metavar="DIR",
                type=str)
    parser.add_argument("-a", "--arucopath",
                help="directory with aruco stuff", metavar="DIR",default = os.path.join(os.path.dirname(os.path.realpath(__file__)),"aruco_templates","2019_03_23"),
                type=str)
    parser.add_argument("--num_frames",
                help="number of frames to process",
                type=int)
    parser.add_argument("--num_cameras",
                help="number of cameras", default = 5,
                type=int)
    parser.add_argument("--camfps",
                help="fps of cameras",  default = 30.,
                type=float)

    parser.add_argument('--nocleanup',help="leave frames in tempdir", action='store_true')
    parser.add_argument('--makevideos',help="create some feedbackvideos", action='store_true')
    parser.add_argument('--recalculate',help="dont skip already finished files but calculate again", action='store_true')

    args = parser.parse_args()


    if args.outputdir is None:
        args.outputdir = args.directory

    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler(os.path.join(args.outputdir,"biglab_videorecognition.log"))
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)


    inputvideos,name = camangle_calibration.get_videos(args.directory,args.num_cameras)

    for vid in inputvideos:
        logger.info(vid)
        process_video(vid,args.arucopath,args.outputdir,args.camfps,args.recalculate,args.makevideos,args.nocleanup,args.num_frames)
