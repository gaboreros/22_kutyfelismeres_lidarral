import os
import numpy as np
import shutil
import glob
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches

import argparse
import subprocess
import sys
import logging
logger = logging.getLogger("merge_logs")


from tempfile import gettempdir

def distance(x1,x2):
    return np.sqrt((x1[0] - x2[0]) * (x1[0] - x2[0]) + (x1[1] - x2[1]) * (x1[1] - x2[1]))

def get_log_data(log_path, processed=True):
    data = []
    with open(log_path, 'r') as logfile:
        for line in logfile.readlines():
            split_line = line.split(',')
            # Is the data processed already?
            if processed:
                bound = len(split_line)
            else:
                bound = len(split_line) - 1
            for i in range(bound):
                split_line[i] = int(split_line[i])
            if not processed:
                split_line[-1] = split_line[-1].split('\n')[0]
            data.append(split_line)
    return data


def get_warped_path(inputpath,outputpath,NUM_CAM = 5,CAMFPS = 30,MAKEVIDEO = False,NOCLEANUP = False):
    log_paths = glob.glob(os.path.join(inputpath,"*_?_warped_log_block.csv"))
    basename = '_'.join(os.path.basename(log_paths[0]).split("_")[:-4])

    if not len(log_paths) == NUM_CAM:
        logger.warning("more logfiles than cameras: {}".format(len(log_paths)))
        # sys.exit(1)

    outlog = os.path.join(outputpath,basename + "_merged_log.csv")
    data_list = []
    for i in range(len(log_paths)):
        data_list.append(get_log_data(log_paths[i]))
    #print(data_list)
    #print("data_list")
    SAVE_FRAMES_TO = os.path.join(gettempdir(), 'temp_merged_frames_{}_{}'.format(basename,hash(os.times())))
    videodirpath = os.path.join(outputpath, "outputvideos")
    OUTPATH = os.path.join(videodirpath, basename +'_merged_path.mp4')
    if MAKEVIDEO:
        logger.info("creating video frames in {}".format(SAVE_FRAMES_TO))
        if not os.path.isdir(videodirpath):
            os.mkdir(videodirpath)
        if os.path.isdir(SAVE_FRAMES_TO):
            shutil.rmtree(SAVE_FRAMES_TO)
        os.mkdir(SAVE_FRAMES_TO)
    IMG_WIDTH = 540
    IMG_HEIGHT = 627
    data_step = [0]*len(data_list)
    data_limit = [len(d_) for d_ in data_list]
    #print(data_list)
    max_frames = np.max(data_limit)
    i = 0
    run = True
    outlogfile = open(outlog,"w")
    framecounter = 0
    while True:
        i += 1
        # pixelsize of the original image
        # print('In frame {0}'.format(i))
        if MAKEVIDEO:
            axes = plt.gca()
            room = patches.Rectangle((0,0),540,627,fill = False)
            axes.add_patch(room)

            axes.set_xlim([0,IMG_WIDTH])
            axes.set_ylim([0,IMG_HEIGHT])
            axes.set_aspect('equal')

            # starting from top left
            plt.style.use('dark_background')
            plt.xlabel("small lab")
            plt.ylabel("windows")


        coords = []
                    
        #for j in range(len(data_list)):
         #   if not data_limit[j] - data_step[j] == 0 and data_list[j][data_step[j]][0] == i:
          #      print((data_list[j][data_step[j]][3]))
                
        for j in range(len(data_list)):
           
            if not data_limit[j] - data_step[j] == 0 and data_list[j][data_step[j]][0] == i: #sorban minden raw-ban bal alsó+jobb felső koordináták
                thiscoord = [(data_list[j][data_step[j]][3] + data_list[j][data_step[j]][5])/2,(data_list[j][data_step[j]][4] + data_list[j][data_step[j]][6])/2]
                coords.append(thiscoord)
                if MAKEVIDEO:
                    plt.plot(data_list[j][data_step[j]][1], data_list[j][data_step[j]][2], marker='o', color=(1,0,0), markersize=3)
                    plt.plot(thiscoord[0],thiscoord[1],marker="o", color="white", markersize = 4)

                data_step[j] = data_step[j] + 1
            else:
                if MAKEVIDEO:
                    plt.plot(35, 5 + j*7, marker='$no data: {0}$'.format(os.path.basename(log_paths[j]).split("_")[-4]), color=(1,1,1), markersize=30)

        avgx = []
        avgy = []
        print(coords)
        for coord in coords:
            avgx.append(coord[0])
            avgy.append(coord[1])

        if not len(avgx) == 0:
            meanx = np.mean(avgx)
            meany = np.mean(avgy)
            if MAKEVIDEO:
                plt.plot(meanx,meany,marker="*", color="yellow", markersize=3)
        else:
            meanx = -1
            meany = -1
        outlogfile.write("{};{:.2f};{:.2f}\n".format(i,meanx,meany))

         #print("a koordinatak:")
        #print(coords)
        #print("az atlag")
        print(meanx)
       
        if MAKEVIDEO:
            plt.savefig(os.path.join(SAVE_FRAMES_TO, str(i) + '.jpg'), bbox_inches='tight', pad_inches=0)
            plt.close('all')


        if np.sum(np.array(data_limit) - np.array(data_step)) == 0:
            break
        framecounter += 1
        if framecounter == 1000:
            print("{} / {}".format(i,max_frames))
            framecounter = 0
    if MAKEVIDEO:
        logger.info("creating video")
        os.system('ffmpeg -y -framerate ' + str(CAMFPS) +' -i ' + SAVE_FRAMES_TO + '/%1d.jpg -vf scale=776:-2 ' + OUTPATH)
        if not NOCLEANUP:
            shutil.rmtree(SAVE_FRAMES_TO)
    logger.info("merging finished")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Merge logs output from process logs.")
    parser.add_argument("-d", "--directory",
                help="directory with input videos", metavar="DIR",
                type=str,required = True)
    parser.add_argument("-o", "--outputdir",
                help="output directory (default is input dir)", metavar="DIR",
                type=str)

    parser.add_argument("--num_cameras",
                help="number of cameras", default = 5,
                type=int)
    parser.add_argument("--camfps",
                help="fps of cameras",  default = 30.,
                type=float)

    parser.add_argument('--nocleanup',help="leave frames in tempdir", action='store_true')
    parser.add_argument('--makevideos',help="create some feedbackvideos", action='store_true')

    args = parser.parse_args()


    if args.outputdir is None:
        args.outputdir = args.directory

    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler(os.path.join(args.outputdir,"biglab_videorecognition.log"))
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    logger.info("merging logs in {}".format(args.directory))

    get_warped_path(args.directory,args.outputdir,args.num_cameras,args.camfps,args.makevideos,args.nocleanup)
