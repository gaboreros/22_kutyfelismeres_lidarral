import subprocess
import argparse
import os
import sys
import logging
logger = logging.getLogger("chain")


parser = argparse.ArgumentParser(description="Process the logs output from process videos.")
#parser.add_argument("-d", "--directory",
 #           help="directory with input videos", metavar="DIR",
  #          type=str,required = True)
parser.add_argument("-d", "--directory",
            help="directory with input videos", metavar="DIR",
            type=str,required = True)

parser.add_argument("-a", "--arucopath",
            help="directory with aruco stuff", metavar="DIR",default = os.path.join(os.path.dirname(os.path.realpath(__file__)),"aruco_templates","2019_03_23"),
            type=str)
parser.add_argument("--num_cameras",
            help="number of cameras", default = 5,
            type=int)

parser.add_argument("--num_frames",
            help="number of frames to process",
            type=int)

parser.add_argument("--camfps",
            help="fps of cameras",  default = 30.,
            type=float)


parser.add_argument('-f','--force',help="force even if automatic aruco test fails", action='store_true')
parser.add_argument('--makevideos',help="create feedbackvideos", action='store_true')
parser.add_argument('--recalculate',help="dont skip already finished files but calculate again", action='store_true')

args = parser.parse_args()


logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
file_handler = logging.FileHandler(os.path.join(args.directory,"biglab_videorecognition.log"))
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)



command = ["python","camangle_calibration.py",
          "-d", args.directory,
          "-a", args.arucopath,
          "--num_cameras", str(args.num_cameras),
          "--camfps", str(args.camfps),
          "--quicktest"
          ]
logger.info("Running aruco test")
output = subprocess.check_output(command).decode("utf-8")
print(output)
output = output.splitlines()[-args.num_cameras:]
output = [float(x.split()[1]) for x in output]
logger.info("aruco test: " + " ".join([str(o) for o in output]))
aruco_isgood = True
for x in output:
    if x < 0.8:
        aruco_isgood = False

if aruco_isgood:
    logger.info("aruco is good, commencing")
elif not aruco_isgood and args.force:
    logger.warning("aruco is bad, but forcing anyway")
else:
    logger.info("aruco is bad, terminating")
    sys.exit()

## run process_videos

command = ["python","process_videos.py",
          "-d", args.directory,
          "-a", args.arucopath,
          "--num_cameras", str(args.num_cameras),
          "--camfps", str(args.camfps),
          ]
if not args.num_frames is None:
    command.append("--num_frames")
    command.append(str(args.num_frames))

if args.recalculate:
    command.append("--recalculate")
if args.makevideos:
    command.append("--makevideos")

retval = subprocess.call(command)
if not retval == 0:
    logger.error("process videos died for some reason, exiting")
    sys.exit()

## run process_logs

command = ["python","process_logs.py",
          "-d", args.directory,
          "-a", args.arucopath,
          "--num_cameras", str(args.num_cameras),
          ]

retval = subprocess.call(command)
if not retval == 0:
    logger.error("process logs died for some reason, exiting")
    sys.exit()

## run merge_logs

command = ["python","merge_logs.py",
          "-d", args.directory,
          "--num_cameras", str(args.num_cameras),
          "--camfps", str(args.camfps),
          ]
if args.makevideos:
    command.append("--makevideos")
retval = subprocess.call(command)

if not retval == 0:
    logger.error("merge logs died for some reason, exiting")
    sys.exit()
