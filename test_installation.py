print("Testing imports")
print("")


import numpy
import skimage.io
import matplotlib
import keras
import cv2
import tensorflow
import subprocess

print("ok")
print("")
print("")

print("Testing Mask_RCNN")
print("")

try:
    from Mask_RCNN import default_model, default_class_names, visualize, visualize_mod
except ImportError:
   raise ImportError('Is the Mask_RCNN folder in your PYTHONPATH? Is the folder named exactly like here?')
print("ok")
print("")
print("")

print("Testing ffmpeg")
print("")

try:
    subprocess.call("ffmpeg")
except FileNotFoundError:
    raise FileNotFoundError('ffmpeg is not installed or not on path')
print("ok")
print("")
print("")


print("Testing pyssim")
print("")

try:
    subprocess.call("pyssim")
except FileNotFoundError:
    raise FileNotFoundError('pyssim is not installed or not on path')
print("ok")
print("")
print("")


print("Testing subprocess")
print("")
try:
    subprocess.call(["python", "-c", "import numpy,cv2,tensorflow"])
except FileNotFoundError:
    raise FileNotFoundError('you need to be able to call this python as python ')
print("ok")
print("")
print("")
