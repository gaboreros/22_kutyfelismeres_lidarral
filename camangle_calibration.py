import subprocess
import glob
import os
import cv2
import argparse
import shutil
import numpy as np
from collections import Counter

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def warp_coordinates(coords, homographies, room_id):
    '''
        We expect the coordinates to be in [width (x), height (y)] format!
        Might modify later to work on the whole log.
    '''
    np_coords = np.float32(coords).reshape(-1,1,2)
    warped_coords = cv2.perspectiveTransform(np_coords, homographies[room_id - 1])
    warped_coords = [list(coord[:][:][0]) for coord in warped_coords]
    return warped_coords

def get_homographies(aruco_data, num_of_angles=5):
    homography_matrices = []
    for i in range(1, num_of_angles + 1):
        aruco_pixelcoords = np.asarray([[aruco[3], aruco[4]] for aruco in aruco_data if aruco[0]==i])
        aruco_realcoords = np.asarray([[aruco[1], aruco[2]] for aruco in aruco_data if aruco[0]==i])
        homography_matrix, _ = cv2.findHomography(aruco_pixelcoords, aruco_realcoords, cv2.RANSAC, 5.0)
        homography_matrices.append(homography_matrix)
    return homography_matrices

def match_aruco(picpath,aruc_path,MAKE_PLOT = False,outpath = '.'):
    template_matching_method = cv2.TM_SQDIFF_NORMED

    min_vals = []

    aruco_data = []
    views = []
    vals = []
    pics = []
    for pic in glob.glob(os.path.join(picpath,"cam?.png")) :
        view,val = recognize_view(pic,aruc_path)
        views.append(view)
        vals.append(val)
        pics.append(pic)

        img = cv2.imread(pic,0)

        for aruco_board in sorted(glob.glob(os.path.join(aruc_path,"{}_*_*.png".format(view)))):

            template = cv2.imread(aruco_board,0)
            w, h = template.shape[::-1]

            # Apply template matching the find the location of the boards
            res = cv2.matchTemplate(img, template, template_matching_method)
            min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

            # We are interested in the location where SQDIFF is minimal
            top_left = min_loc
            min_vals.append(min_val)
            bottom_right = (top_left[0] + w, top_left[1] + h)
            cv2.rectangle(img,top_left, bottom_right, 255, 2)
            x = (top_left[0] + bottom_right[0])//2
            y = (top_left[1] + bottom_right[1])//2
            cam,X,Y = os.path.splitext(os.path.basename(aruco_board))[0].split("_")[:5]
            aruco_data.append([view,int(X),int(Y),x,y])

        if MAKE_PLOT:
            fig = plt.figure()
            plt.imshow(img,cmap = 'gray')
            plt.savefig(os.path.join(outpath,"tags_" + os.path.basename(pic)))

            # ROOM_ID, ARUC middle Y COORD, ARUC middle X COORD
            #toprint = ','.join(aruco_board.split('_'))[:5] + ',' + str((top_left[0] + bottom_right[0])//2)  + ',' + str((top_left[1] + bottom_right[1])//2) + '\n'
            #print('Writing {0} to {1}'.format(toprint[:-1]))

    return aruco_data,views,vals,pics

def get_videos(d,N):
    vids = glob.glob(os.path.join(d,"*_?.mp4"))
    names = Counter(["_".join(os.path.basename(v).split("_")[:-1]) for v in vids])

    if list(names.values()).count(N) != 1:
        raise FileNotFoundError("too many videos found {}".format(names))

    name = list(names.keys())[list(names.values()).index(N)]
    vids = glob.glob(os.path.join(d,name + "_?.mp4"))

    inputset = set([int(os.path.basename(v).split("_")[-1].split(".mp4")[0]) for v in vids])
    fullset = set([x for x in range(6)])
    if fullset.issubset(inputset):
        raise FileNotFoundError("format not ok")

    return sorted(vids),name

def check_camera_angle_compatibility(videopath, arucopath):
    os.system('ffmpeg -i ' +  VIDEO_PATH +  ' -r 30 -f image2 ./frames/%1d.png')


def recognize_view(img_path, arucopath, verbose=False, make_plot=False):
    '''
        Recognize the room by applying template matching to rectangle patches in
        the middle of the given image. Works for the time being, might need
        a better solution. The image given is usually the first shot of the frame.
        We assume the dimensions of given 'img' and 'cam#.jpg' are the same!
    '''
    angle_means = []
    CAM_ANGLE_DIR = arucopath
    for camangle in sorted(glob.glob(os.path.join(CAM_ANGLE_DIR,"cam?.jpg")))  + sorted(glob.glob(os.path.join(CAM_ANGLE_DIR,"cam?.png"))):

        angle_means.append(float(subprocess.check_output(['pyssim', img_path, camangle])))

    return angle_means.index(max(angle_means)) + 1,max(angle_means)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Check camera setup.")
    parser.add_argument("-d", "--directory",
                help="directory with input videos", metavar="DIR",
                type=str,required = True)
    parser.add_argument("-o", "--outputdir",
                help="output directory (default is input dir)", metavar="DIR",
                type=str)

    parser.add_argument("-a", "--arucopath",
                help="directory with aruco stuff", metavar="DIR",default = os.path.join(os.path.dirname(os.path.realpath(__file__)),"aruco_templates","2019_03_23"),
                type=str)
    parser.add_argument("--num_cameras",
                help="number of cameras", default = 5,
                type=int)
    parser.add_argument("--camfps",
                help="fps of cameras",  default = 30.,
                type=float)


    action = parser.add_mutually_exclusive_group(required=True)
    action.add_argument('--quicktest', action='store_true', help='check first video frames')
    action.add_argument('--test', action='store_true', help='supply your own frames')
    action.add_argument('--get_all_frames', action='store_true', help='unpacks the videos')
    action.add_argument('--generate_aruco', action='store_true', help='generate new aruco setup')
    action.add_argument("--get_num_frames", help="number of frames to get", type=int)

    args = parser.parse_args()

    if args.quicktest:
        inputvideos,name = get_videos(args.directory,args.num_cameras)

        print("working on videos:")
        print("tag:", name)
        for v in inputvideos:
            print(v)
        print("")
        if args.outputdir is None:
            args.outputdir = args.directory

        framesdir = os.path.join(args.outputdir,"frames")
        if os.path.isdir(framesdir):
            shutil.rmtree(framesdir)
        os.mkdir(framesdir)
        for i,v in enumerate(inputvideos):
            subprocess.call('ffmpeg -hide_banner -loglevel panic -i "{}" -r {} -vframes 1 -f image2 "{}"'.format(v,args.camfps,os.path.join(framesdir,'cam' + str(i + 1)+ ".png")),shell=True)
        aruco_data,views,vals,pics = match_aruco(framesdir,args.arucopath,MAKE_PLOT = True,outpath = args.outputdir)
        for vi,va,pi in zip(views,vals,pics):
            print(vi,va,os.path.basename(pi))

    elif args.get_all_frames:
        inputvideos,name = get_videos(args.directory,args.num_cameras)

        print("working on videos:")
        print("tag:", name)
        for v in inputvideos:
            print(v)
        print("")
        if args.outputdir is None:
            args.outputdir = args.directory

        framesdir = os.path.join(args.outputdir,"frames")
        if os.path.isdir(framesdir):
            shutil.rmtree(framesdir)
        os.mkdir(framesdir)
        for i,v in enumerate(inputvideos):
            subprocess.call('ffmpeg  -i "{}" -r {}  -f image2 "{}"'.format(v,args.camfps,os.path.join(framesdir,'cam'+ str(i + 1) +'_%5d.png')),shell=True)

    elif not args.get_num_frames is None:
        inputvideos,name = get_videos(args.directory,args.num_cameras)

        print("working on videos:")
        print("tag:", name)
        for v in inputvideos:
            print(v)
        print("")
        if args.outputdir is None:
            args.outputdir = args.directory

        framesdir = os.path.join(args.outputdir,"frames")
        if os.path.isdir(framesdir):
            shutil.rmtree(framesdir)
        os.mkdir(framesdir)
        for i,v in enumerate(inputvideos):
            subprocess.call('ffmpeg  -i "{}" -r {} -vframes {} -f image2 "{}"'.format(v,args.camfps,args.get_num_frames,os.path.join(framesdir,'cam'+ str(i + 1) +'_%5d.png')),shell=True)



    elif args.test:
        inputvideos,name = get_videos(args.directory,args.num_cameras)

        print("working on videos:")
        print("tag:", name)
        for v in inputvideos:
            print(v)
        print("")
        if args.outputdir is None:
            args.outputdir = args.directory

        framesdir = os.path.join(args.outputdir,"frames")


        aruco_data,views,vals,pics = match_aruco(framesdir,args.arucopath,MAKE_PLOT = True,outpath = args.outputdir)
        for vi,va,pi in zip(views,vals,pics):
            print(vi,va,os.path.basename(pi))

    elif args.generate_aruco:
        if args.outputdir is None:
            args.outputdir = args.directory

        outpath = os.path.join(args.outputdir,"views")
        if os.path.isdir(outpath):
            shutil.rmtree(outpath)
        os.mkdir(outpath)
        aruco_data,views,vals,pics = match_aruco(args.directory,args.directory,MAKE_PLOT = True,outpath = outpath)
        np.savetxt(os.path.join(args.outputdir,"aruco_locations.csv"),aruco_data,delimiter=",",fmt='%d')
        homographies = get_homographies(aruco_data,args.num_cameras)
        doors = np.array([
                    [ 1.6978143,  -0.12388379],
                    [ 1.0919724,  -0.12954395],
                    [5.4074225, 5.208881 ],
                    [5.4024954, 4.2819815]
                ])
        hatches = ['/', '*', '\\', '.', 'O','X']
        colors = ['red','blue','green','black','orange','yellow']
        Ly = 492
        Lx = 658
        N = 10
        corners = [[0,y_] for y_ in np.linspace(0,Ly,N)] \
                   + [[x_,Ly] for x_ in np.linspace(0,Lx,int(round(N/Ly*Lx)))] \
                    + [[Lx,y_] for y_ in np.linspace(Ly,0,N)] \
                    + [[x_,0] for x_ in np.linspace(Lx,0,int(round(N/Ly*Lx)))]
        room = patches.Rectangle((0,0),540,627,fill = False)

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.add_patch(room)

        ax.set_xlim([-100,700])
        ax.set_ylim([-100,700])
        ax.set_aspect('equal')
        plt.text(70,-80,"door to small lab",bbox=dict(facecolor='white'))
        plt.text(570,580,"door to corridor",bbox=dict(facecolor='white'),rotation = -90)

        plt.scatter(doors[:,0]*100,doors[:,1]*100,color="black")
        for h,c,hatch in zip(homographies,colors,hatches):
            d = cv2.perspectiveTransform(np.float32(corners).reshape(-1,1,2),h)

            cam = patches.Polygon(d[:,0,:]*100,alpha = 0.2,color = c,hatch = hatch)
            ax.add_patch(cam)
        plt.savefig(os.path.join(outpath,'camview_full_hatched.jpg'))

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        room = patches.Rectangle((0,0),540,627,fill = False)
        ax.add_patch(room)

        ax.set_xlim([-100,700])
        ax.set_ylim([-100,700])
        ax.set_aspect('equal')
        plt.text(70,-80,"door to small lab",bbox=dict(facecolor='white'))
        plt.text(570,580,"door to corridor",bbox=dict(facecolor='white'),rotation = -90)

        plt.scatter(doors[:,0]*100,doors[:,1]*100,color="black")
        for h,c in zip(homographies,colors):
            d = cv2.perspectiveTransform(np.float32(corners).reshape(-1,1,2),h)

            cam = patches.Polygon(d[:,0,:]*100,alpha = 0.2,color = c)
            ax.add_patch(cam)
        plt.savefig(os.path.join(outpath,'camview_full.jpg'))
