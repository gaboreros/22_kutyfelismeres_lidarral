#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function

import glob
import pickle
import cv2
import numpy as np
from keras.models import Model, load_model
from keras.layers import Input, concatenate, Conv2D, MaxPooling2D, UpSampling2D
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler
from keras import backend as K
from keras.utils import multi_gpu_model

import sys
K.set_image_data_format('channels_last')

# Tamaño de las imagenes
img_rows = 256
img_cols = 256
smooth = 1.

# Métricas personalizada
def dice_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)

# Creación del modelo
def get_unet():

    inputs = Input((img_rows, img_cols, 1))
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(inputs)
    conv1 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv1)
    pool1 = MaxPooling2D(pool_size=(8, 8))(conv1)

    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(pool1)
    conv2 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv2)
    pool2 = MaxPooling2D(pool_size=(8, 8))(conv2)

    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(pool2)
    conv3 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv3)
#    pool3 = MaxPooling2D(pool_size=(6, 6))(conv3)

#    conv4 = Conv2D(256, (3, 3), activation='relu', padding='same')(pool3)
#    conv4 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv4)
#    pool4 = MaxPooling2D(pool_size=(6, 6))(conv4)

#    conv5 = Conv2D(512, (3, 3), activation='relu', padding='same')(pool4)
#    conv5 = Conv2D(512, (3, 3), activation='relu', padding='same')(conv5)

#    up6 = concatenate([UpSampling2D(size=(6, 6))(conv5), conv4], axis=3)
#    conv6 = Conv2D(256, (3, 3), activation='relu', padding='same')(up6)
#    conv6 = Conv2D(256, (3, 3), activation='relu', padding='same')(conv6)

#    up7 = concatenate([UpSampling2D(size=(6, 6))(conv4), conv3], axis=3)
#    conv7 = Conv2D(128, (3, 3), activation='relu', padding='same')(up7)
#    conv7 = Conv2D(128, (3, 3), activation='relu', padding='same')(conv7)

    up8 = concatenate([UpSampling2D(size=(8, 8))(conv3), conv2], axis=3)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(up8)
    conv8 = Conv2D(64, (3, 3), activation='relu', padding='same')(conv8)

    up9 = concatenate([UpSampling2D(size=(8, 8))(conv8), conv1], axis=3)
    conv9 = Conv2D(32, (3, 3), activation='relu', padding='same')(up9)
    conv9 = Conv2D(32, (3, 3), activation='relu', padding='same')(conv9)

    conv10 = Conv2D(1, (1, 1), activation='sigmoid')(conv9)

    model = Model(inputs=inputs, outputs=conv10)

#    model.compile(optimizer=Adam(lr=1e-5), loss=dice_coef_loss, metrics=[dice_coef])

    parallel_model = multi_gpu_model(model, gpus=4)

    parallel_model.compile(optimizer=Adam(lr=1e-5), loss=dice_coef_loss, metrics=[dice_coef])

#    return model
    return parallel_model

# Preprocesamiento de imagenes. Pasamos de (x, 512, 512) a (x, 512, 512, 1)
def preprocess(imgs):
    imgs_p = np.ndarray((imgs.shape[0], img_rows, img_cols, imgs.shape[1]), dtype=np.uint8)
    for i in range(imgs.shape[0]):
        imagen = cv2.resize(imgs[i, 0], (256, 256), interpolation=cv2.INTER_CUBIC)
        #image_crop = imagen[256:512, 0:512]
        #image_crop = image_crop[0:256, 128:384]
        #imgs_p[i,:,:,0] = image_crop
        imgs_p[i,:,:,0] = imagen
    return imgs_p

# Entrenamiento y test
def train_and_predict(args):
    print('-'*50)
    print('Cargando y Preprocesando datos para entrenar')
    print('-'*50)

    # Cargamos los datos de entrenamiento
    #train_points = np.load(args[1] + '/input/output_raw_frames.npy')
    #train_labels = np.load(args[1] + '/input/output_label_frames.npy')
    raw_files = sorted(glob.glob(args[1] + '/input/*_raw_frames_full.npy'))
    label_files = sorted(glob.glob(args[1] +  '/input/*_label_frames.npy'))
    poi_files = sorted(glob.glob(args[1] +  '/input/*_poi_frames_full.pkl'))
    #print(len(raw_files)) #üreset olvas be??

    points = np.load(raw_files[0])
    labels = np.load(label_files[0])
    for rawf in raw_files[1:]:
        points = np.concatenate((points,np.load(rawf)))
    for labelf in label_files[1:]:
        labels = np.concatenate((labels,np.load(labelf)))

    pois = []
    for poif in poi_files:
        pois += pickle.load(open(poif,"rb"),encoding = "bytes") # needed because original pickle is from python2
    print(len(points),len(labels),len(pois))

    #print(pois[0])
    indices = np.random.permutation(points.shape[0])
    divider = int(len(indices)*0.67)
    training_idx, test_idx = indices[:divider], indices[divider:]
    train_points, test_points = points[training_idx,:], points[test_idx,:]
    train_labels, test_labels = labels[training_idx,:], labels[test_idx,:]
    train_pois = []
    #print(pois[0])
    for i in training_idx:
        train_pois.append(pois[i])
    test_pois = []
    for i in test_idx:
        test_pois.append(pois[i])

    # Preprocesamos los datos de entrenamiento
    train_points = preprocess(train_points)
    train_labels = preprocess(train_labels)

    print('-'*50)
    print('Tamanio de los datos de entrenamiento: ',train_points.shape)
    print('-'*50)

    # Cambiamos a tipo float los datos
    train_points = train_points.astype('float32')
    mean = np.mean(train_points)  # mean for data centering
    std = np.std(train_points)  # std for data normalization

    train_points -= mean
    train_points /= std

    train_labels = train_labels.astype('float32')

    print('-'*50)
    print('Creando y compilando el modelo')
    print('-'*50)
    # Si es la primera vez que se entrena la red
    model = get_unet()
    # Si se quiere entrenar sobre un modelo ya entrenado
    #model = load_model(args[1] + '/modelo/modelo_'+ args[2] +'.h5', custom_objects={'dice_coef_loss': dice_coef_loss, 'dice_coef': dice_coef})
    # Los datos de los pesos se van almacenando en cada iteración en el archivo modelo.h5
    model_checkpoint = ModelCheckpoint(args[1] + '/modelo/modelo_'+ args[2] +'.h5', monitor='loss', save_best_only=True)

    print('-'*50)
    print('Entrenando el modelo')
    print('-'*50)
    model.fit(train_points, train_labels, batch_size=16, epochs=40, verbose=1, shuffle=True,
              callbacks=[model_checkpoint])

    print('-'*50)
    print('Cargando y preprocesando datos de test')
    print('-'*50)

#    test_points = np.load(args[1] + '/input/npy_total_test_points.npy')
    test_points = preprocess(test_points)

    print('-'*50)
    print('Tamanio de los datos de test: ', test_points.shape)
    print('-'*50)

    test_points = test_points.astype('float32')
    mean = np.mean(test_points)  # mean for data centering
    std = np.std(test_points)  # std for data normalization

    test_points -= mean
    test_points /= std

    print('-'*50)
    print('Cargando los pesos guardados')
    print('-'*50)
    model.load_weights(args[1] + '/modelo/modelo_'+ args[2] +'.h5')

    print('-'*50)
    print('Prediccion de los datos de test')
    print('-'*50)
    test_labels_generated = model.predict(test_points, verbose=1)
    # Guardamos los datos de salida para la entrada de test
    np.save(args[1] + '/output/test_labels_' + args[2], test_labels_generated)
    np.save(args[1] + '/output/test_points_' + args[2], test_points)
    pickle.dump(test_pois,open(args[1] + '/output/test_pois_' + args[2] + ".pkl","wb"))

if __name__ == '__main__':
    train_and_predict(sys.argv)
