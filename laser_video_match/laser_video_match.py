#!/usr/bin/env python
from __future__ import print_function,division,with_statement

#import roslibpy
#import roslib
#import rospy
import rosbag
#ThreadingMixIn helyette
#import pyrosbag
#import tf
#import pyrosbag

#from geometry_msgs.msg import PointStamped
#from tf2_msgs.msg import TFMessage

import subprocess, yaml #required to check bag file contents
import numpy as np
from scipy.spatial.transform import Rotation

import skimage.io
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
from random import randint

import argparse
import os
import sys
import shutil
import glob
import time
import gc
import pickle

from tempfile import gettempdir

from detector_lib import get_pixel_in_matrix

L = 256


'''
The logic of tf messages is how to go UP the stack.

To go up the stack you need to:
r.apply(p) + t

To go down the stack you need the inverse:
r.inv().apply((p - t))
or to put the previous notation
r' = r.inv()
t' = - r.inv(t)
r'.apply(p) + t'
'''

def get_transform(bag,from_,to_):

    # get all possible transform messages
    transforms = []
    counter = 0
    for topic, msg, t in bag.read_messages(topics='/tf'):
        transforms += msg.transforms
        counter += 1
        if counter == 50:
            break

    for topic, msg, t in bag.read_messages(topics='/tf_static'):
        transforms += msg.transforms


    # go up the tree
    # from_ should be somewhere up and to_ should be somewhere down
    # eg: from_ is map to_ is laser
    item = to_
    consecutive_transforms = []
    while not item == from_:
        item = next( (x for x in transforms if x.child_frame_id == item))
        consecutive_transforms.append(item.transform)
        item = item.header.frame_id

    # we will need to invert the transforms since we want to go down the stack.
    consecutive_transforms = [[np.array([
                                c.translation.x,
                                c.translation.y,
                                c.translation.z
                                ]),
                                Rotation.from_quat(np.array([
                                c.rotation.x,
                                c.rotation.y,
                                c.rotation.z,
                                c.rotation.w
                                ]))
                                ]
                for c in consecutive_transforms]

    # going up the stack

    r = consecutive_transforms[-1][1].inv()
    t = -1 * r.apply(consecutive_transforms[-1][0])
    for tf_ in reversed(consecutive_transforms[:-1]):
        r = tf_[1].inv()*r
        t = tf_[1].inv().apply(t) - tf_[1].inv().apply(tf_[0])

    return t,r

'''
For argparse to see if the argument is actually a file.
'''
def is_valid_file(parser, arg):

    if not os.path.exists(arg):
        parser.error("The file {} does not exist!".format(arg))
    else:
        return arg  # return an open file handle



'''
Translate a laser message frame to a matrix.
'''
def frame_to_pic(msg,poi,poi_fps,syncpoint,intensity_threshold = 0):
    timestamp = msg.header.stamp
    poi_int = int((timestamp.secs + timestamp.nsecs/1000000000 - syncpoint)*poi_fps)
    matrix_image_raw = np.zeros((L,L))

    try:
        poi_line = poi[poi_int]
        poi_line = [poi_line[1 + i*3 : 4 + i*3] for i in range((len(poi_line) - 1)//3)]

    except IndexError:
        poi_line = [-10000,-10000,0]
    coords_x = []
    coords_y = []
    matrix_image_raw = np.zeros((L,L))
    for i,(r,intensity) in enumerate(zip(msg.ranges,msg.intensities)):
        if intensity > intensity_threshold:
            phi = msg.angle_min + (i-1)*msg.angle_increment
            x = r*np.cos(phi)
            y = r*np.sin(phi)
            coords_x.append(x)
            coords_y.append(y)
            j,k = get_pixel_in_matrix(x,y)
            if j>=0 and j<L and k>=0 and k<L:
                matrix_image_raw[j][k] = 1


    # skimage.io.imsave("output/{}_label.jpg".format(msg.header.seq),matrix_image_raw)
    return coords_x,coords_y,poi_line,matrix_image_raw

'''
This loads the coordinates of the dog in the lab map reference frame and uses some predetermined points
to calculate a transform from the lab map into the ros map.
'''
def load_point_of_interest(poi,rosmap,labmap,t,r,dogs,humans):
    rosmapcoords = np.loadtxt(rosmap, delimiter=";")
    labmapcoords = np.loadtxt(labmap, delimiter=";")
    homog,_ = cv2.findHomography(labmapcoords,rosmapcoords)
    retval = []
    with open(poi) as f:
        for line in f:
            line = line.split(";")
            for i in range(len(line)):
                try:
                    line[i] = int(line[i])
                except ValueError:
                    line[i] = float(line[i])
            keepers = []
            for i in range((len(line)-1)//3):
                if line[3+i*3] == 1 and dogs:
                    keepers.append(i)

                if line[3+i*3] == 2 and humans:
                    keepers.append(i)
            filteredline = [line[0]]
            for i in keepers:
                filteredline += line[1+i*3:4+i*3]
            line = filteredline
            for i in range((len(line)-1)//3):
                coord = np.zeros((1,3))
                coord[:,:-1] =  cv2.perspectiveTransform(np.array(line[1 + i*3:3 + i*3]).reshape(-1,1,2),homog)[:,0][0]

                coord = (r.apply(coord) + t)[:,:2][0]

                line[1 + i*3] = coord[0]
                line[2 + i*3] = coord[1]

            retval.append(line)
    print("loadtest",retval)
    return retval


def create_circular_mask(h, w, center=None, radius=None):

    if center is None: # use the middle of the image
        center = [int(w/2), int(h/2)]
    if radius is None: # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w-center[0], h-center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)

    mask = dist_from_center <= radius
    return mask

def get_laser_frames(rosbagpath,
                    poi,
                    syncpoint_laser,
                    syncpoint_videotime,
                    topics = '/rb1_base/front_laser/scan',
                    intensity_threshold = 0,
                    video_frame_per_sec = 30):
    baginfo = yaml.load(subprocess.Popen(['rosbag', 'info', '--yaml', rosbagpath], stdout=subprocess.PIPE).communicate()[0])
    numitems = next((x for x in baginfo['topics'] if x["topic"] == '/rb1_base/front_laser/scan'))["messages"]
    bag = rosbag.Bag(rosbagpath)
    laser_frames = []
    counter = 0
    progress = 0
    poi_frames = []
    laser_x_list = []
    laser_y_list = []
    for topic, msg, t in bag.read_messages(topics=topics):
        laser_x,laser_y,poi_points,laser_frame = frame_to_pic(
                                        msg,
                                        poi,
                                        video_frame_per_sec,
                                        syncpoint_laser - syncpoint_videotime,
                                        intensity_threshold = intensity_threshold)
        laser_frames.append(laser_frame)
        poi_frames.append(poi_points)
        laser_x_list.append(laser_x)
        laser_y_list.append(laser_y)


        counter += 1
        if counter == args.stop_at_frame:
            print("Stopping at user instructed laser frame No. {}".format(counter))
            break
        progress += 1
        if progress == 100:
            progress = 0
            print("{}/{}".format(counter,numitems))
    return laser_x_list,laser_y_list,laser_frames,poi_frames

def get_masked_frames(laser_frames,window,maskthreshold):
    masked_frames = []
    counter = 0
    progress = 0
    numitems = len(laser_frames)
    if window > len(laser_frames):
        window = len(laser_frames)
    if window > 0:
        mask = np.sum(laser_frames[:window],axis = 0)
        for i in range(len(laser_frames)):
            if i < window/2:
                masked_frames.append(mask/window > maskthreshold)
            elif i >= len(laser_frames) - window/2:
                masked_frames.append(mask/window > maskthreshold)
            else:
                mask += laser_frames[int(i + window/2)] - laser_frames[int(i - window/2)]
                masked_frames.append(mask/window > maskthreshold)
            counter += 1
            progress += 1
            if progress == 100:
                progress = 0
                print("{}/{}".format(counter,numitems))
    else:
        for i in range(len(laser_frames)):
            masked_frames.append(np.zeros((L,L)))
    return masked_frames


def apply_poi_on_frames(laser_frames,masked_frames,poi_frames,radius_for_poi,min_num_pixels):
    progress = 0
    counter = 0
    isframenotempty = []
    numitems = len(laser_frames)
    for i,(f,m,poi_points) in enumerate(zip(laser_frames,masked_frames,poi_frames)):
        # we need to dilate it slightly because it's hard to get single pixels
        labelmatrix = (f - cv2.dilate(m.astype(np.uint8),np.ones((3,3))) > 0).astype(int)

        # now we apply the pois
        mask1 = np.zeros((L,L))
        mask2 = np.zeros((L,L))
        for pp in poi_points:

            x,y = get_pixel_in_matrix(pp[0],pp[1])
            if pp[2] == 1:
                mask1 = np.logical_or(mask1,create_circular_mask(L,L,(x,y),radius_for_poi*100/2))
            elif pp[2] == 2:
                mask2 = np.logical_or(mask2,create_circular_mask(L,L,(x,y),radius_for_poi*100/2))
            else:
                raise ValueError("poi not 1 or 2")

        labelmatrix1 = np.logical_and(labelmatrix,mask1)
        labelmatrix2 = np.logical_and(labelmatrix,mask2)


        isgoodframe = np.sum(np.logical_or(labelmatrix1,labelmatrix2)) >= min_num_pixels
        isframenotempty.append(isgoodframe)


        # TODO: check here if there's anything left if not, write on the image
        # for the visual feedback and set up a counter so we know long
        # array should be allocated later

        masked_frames[i] = labelmatrix1 + labelmatrix2
        progress += 1
        counter += 1
        if progress == 100:
            progress = 0
            print("{}/{}".format(counter,numitems))
    return masked_frames,isframenotempty

'''
I'm unable to clean memory otherwise, and subprocesses spawn forks
and forks allocate the same amount of memory as the process spawning them or
something like that and since we have huge amounts of data we must clear before that.
'''
def main(q):
    args = q.get()


    bag = rosbag.Bag(args.rosbag)
    t,r = get_transform(bag,'rb1_base_map','rb1_base_front_laser_link')
    SAVE_FRAMES_TO_LABEL = os.path.join(gettempdir(), 'lasermatch_label_{}'.format(hash(os.times())))
    SAVE_FRAMES_TO_SYNC = os.path.join(gettempdir(), 'lasermatch_synctest_{}'.format(hash(os.times())))
    SAVE_FRAMES_TO_RAW = os.path.join(gettempdir(), 'lasermatch_raw_{}'.format(hash(os.times())))

    if args.visualfeedback:


        if os.path.isdir(SAVE_FRAMES_TO_LABEL):
            shutil.rmtree(SAVE_FRAMES_TO_LABEL)
        os.mkdir(SAVE_FRAMES_TO_LABEL)

        if os.path.isdir(SAVE_FRAMES_TO_SYNC):
            shutil.rmtree(SAVE_FRAMES_TO_SYNC)
        os.mkdir(SAVE_FRAMES_TO_SYNC)

        if os.path.isdir(SAVE_FRAMES_TO_RAW):
            shutil.rmtree(SAVE_FRAMES_TO_RAW)
        os.mkdir(SAVE_FRAMES_TO_RAW)

        rosmapcoords = np.loadtxt(args.points_ros_map, delimiter=";")
        corners = np.zeros((4,3))
        corners[:,:-1] = rosmapcoords[:4]
        corners = (r.apply(corners) + t)[:,:2]
        xmin = np.min(corners[:,0]) - 1
        xmax = np.max(corners[:,0]) + 1
        ymin = np.min(corners[:,1]) - 1
        ymax = np.max(corners[:,1]) + 1

    poi = load_point_of_interest(args.poi,args.points_ros_map,args.points_lab_map,t,r,args.dogs, args.humans)
    print("bag and transforms loaded")


    '''
    We get the syncpoint by finding the end of the covering phase
    At this point the points in the middle of the range should be much-much
    closer than normally, so we check in the middle of this ranges

    '''
    already = False
    syncpoint_laser = 0
    window = 0
    for topic, msg, t in bag.read_messages(topics='/rb1_base/front_laser/scan'):
        rangelen = int(np.round(len(msg.ranges)/4.))
        window = int(np.round(args.window_length/msg.scan_time))
        if np.mean(msg.ranges[rangelen:-rangelen]) < args.syncthreshold:
            timestamp = msg.header.stamp
            syncpoint_laser = timestamp.secs + timestamp.nsecs/1000000000
            already = True
        else:
            if already:
                break
    if syncpoint_laser == 0:
        print("no sync in laser found, terminating")
        sys.exit()
    print("syncpoint is {} at {}".format(syncpoint_laser,args.sync_videotime))
    print("window is {} laser frames long".format(window))



    laser_x_list,laser_y_list,laser_frames, poi_frames = get_laser_frames(
                        args.rosbag,
                        poi,
                        syncpoint_laser,
                        args.sync_videotime,
                        intensity_threshold = args.intensity_threshold,
                        video_frame_per_sec = args.video_frame_per_sec)
    # create sync test
    if args.visualfeedback:
        print("creating sync test and raw video")
        progress = 0
        counter = 0
        numitems = len(laser_frames)
        i_ = 0
        for laser_frame,poi_points,laser_x,laser_y in zip(laser_frames,poi_frames,laser_x_list,laser_y_list):
            if divmod(counter,4)[1] == 0:
                i_ += 1
                fig = plt.figure()
                ax = fig.add_subplot(1, 1, 1)
                ax.set_xlim([xmin,xmax])
                ax.set_ylim([ymin,ymax])
                room = patches.Polygon(corners,fill=False,alpha = .2, color = "red")
                ax.add_patch(room)
                ax.set_aspect('equal')
                plt.scatter(laser_x,laser_y,marker=".",s = 1)
                for pp in poi_points:
                    color = "red" if pp[2] == 1 else "orange"
                    plt.scatter(pp[0],pp[1],marker="*", s = 3,c = color)
                    poicircle = patches.Circle([pp[0],pp[1]],radius=args.radius_for_poi,fill=True,alpha = 0.2, color = color)
                    ax.add_patch(poicircle)
                plt.savefig(os.path.join(SAVE_FRAMES_TO_SYNC,'{:05d}'.format(i_)))



                # create raw test
                fig = plt.figure()
                plt.matshow(laser_frame)
                plt.savefig(os.path.join(SAVE_FRAMES_TO_RAW,'{:05d}'.format(i_)))
                plt.close('all')
            counter += 1
            progress += 1
            if progress == 100:
                progress = 0
                print("{}/{}".format(counter,numitems))
    '''
    Any point that appears on args.maskthreshold portion of the laserframes
    should be conidered as background. If window is 0, don't do anything.
    '''

    print("calculating windows")
    masked_frames = get_masked_frames(laser_frames,window,args.maskthreshold)

    print("making label images")
    masked_frames,isframenotempty = apply_poi_on_frames(laser_frames,masked_frames,poi_frames,args.radius_for_poi,args.min_num_pixels)

    if args.visualfeedback:

        print("creating label video")
        progress = 0
        counter = 0
        numitems = len(masked_frames)
        i_ = 0
        for labelmatrix,isgoodframe in zip(masked_frames,isframenotempty):
            # create raw test
            if divmod(counter,4)[1] == 0:
                i_ += 1
                fig = plt.figure()
                plt.matshow(labelmatrix)
                if not isgoodframe:
                    plt.text(L/2,L/2,"frame not used",horizontalalignment='center',fontsize = 24)
                plt.savefig(os.path.join(SAVE_FRAMES_TO_LABEL,'{:05d}'.format(i_)))
                plt.close('all')
            counter += 1
            progress += 1
            if progress == 100:
                progress = 0
                print("{}/{}".format(counter,numitems))

    print("saving results to file")

    num_final_frames = np.sum(isframenotempty)
    npy_data_raw_full = np.ndarray((num_final_frames, 1, L, L), dtype=np.uint8)
    icounter = 0
    for i,f in enumerate(laser_frames):
        if isframenotempty[i]:
            npy_data_raw_full[icounter] = f
            icounter += 1

    np.save(os.path.join(args.outputdirectory,args.name + "_raw_frames.npy"), npy_data_raw_full)
    del npy_data_raw_full
    np.save(os.path.join(args.outputdirectory,args.name + "_raw_frames_full.npy"), np.expand_dims(laser_frames,axis = 1))
    del laser_frames

    npy_data_label_full = np.ndarray((num_final_frames, 1, L, L), dtype=np.uint8)
    icounter = 0
    for i,f in enumerate(masked_frames):
        if isframenotempty[i]:
            npy_data_label_full[icounter] = f
            icounter += 1

    np.save(os.path.join(args.outputdirectory,args.name+ "_label_frames.npy"), npy_data_label_full)
    del npy_data_label_full

    poi_frames_final = []
    for i,p in enumerate(poi_frames):
        if isframenotempty[i]:
            poi_frames_final.append(p)

    pickle.dump(poi_frames_final,open(os.path.join(args.outputdirectory,args.name+ "_poi_frames.pkl"),"w"))
    pickle.dump(poi_frames,open(os.path.join(args.outputdirectory,args.name+ "_poi_frames_full.pkl"),"w"))

    # del npy_data_raw_full
    # del npy_data_label_full
    # del laser_frames
    # del masked_frames
    # del poi_frames
    # print("sleep 1")
    # time.sleep(30)

    q.put((SAVE_FRAMES_TO_LABEL, SAVE_FRAMES_TO_SYNC, SAVE_FRAMES_TO_RAW))


if __name__ == "__main__":
    print("starting up")
    parser = argparse.ArgumentParser(description="Match laser data with lab camera output.")
    parser.add_argument("-b", "--rosbag",
                    help="input rosbag", metavar="FILE",
                    type=lambda x: is_valid_file(parser, x))
    parser.add_argument("-p", "--poi",
                    help="input file that holds positions of interest in lab space", metavar="FILE",
                    type=lambda x: is_valid_file(parser, x))

    parser.add_argument("-s", "--sync_videotime",
                    help="input file that holds the time of the videoframe for the syncpoint", metavar="FILE",
                    type=lambda x: is_valid_file(parser, x))

    parser.add_argument("-d", "--directory",
                    help="alternatively, give input directory", metavar="DIR",
                    type=str)

    parser.add_argument("-o", "--outputdirectory",
                    help="if unspecified will be same as inputdir, or current dir if input dir is not specified", metavar="FILE",
                    type=str)
    parser.add_argument("-n","--name",
                    help="prepend filenames with this",
                    type=str)

    parser.add_argument("-map", "--points_ros_map",
                    help="ros map calibration points", metavar="FILE", default="ros_map_coordinates.csv",
                    type=lambda x: is_valid_file(parser, x))

    parser.add_argument("-lab", "--points_lab_map",
                    help="lab map calibration points", metavar="FILE", default="lab_map_coordinates.csv",
                    type=lambda x: is_valid_file(parser, x))


    parser.add_argument("-fps", "--video_frame_per_sec",
                    help="transform from laser to map", default=30,
                    type=int)

    parser.add_argument("-r", "--radius_for_poi",
                    help="radius in meters", default=0.6,
                    type=float)

    parser.add_argument("-mp","--min_num_pixels",
                    help="minimum number of pixels in label image", default=10,
                    type=int)


    parser.add_argument("-sf","--stop_at_frame",
                    help="default is process all", default=-1,
                    type=int)

    parser.add_argument("-st","--syncthreshold",
                    help="threshold for sync", default=0.5,
                    type=float)

    parser.add_argument("-mt","--maskthreshold",
                    help="threshold for masking", default=0.5,
                    type=float)

    parser.add_argument("-it","--intensity_threshold",
                    help="threshold for intensity of laser", default=0,
                    type=float)

    parser.add_argument("-w","--window_length",
                    help="window length in seconds", default=90,
                    type=float)

    parser.add_argument("--dogs",
                    help="process dogs",action="store_true")

    parser.add_argument("--humans",
                    help="process humans",action="store_true")

    parser.add_argument('--nocleanup', action='store_true')
    parser.add_argument('-vf','--visualfeedback', action='store_true')

    args = parser.parse_args()

    if args.directory is None:
        if args.rosbag is None or args.poi is None or args.sync_videotime is None:
            print("please specify directory of rosbag and poi file")
            sys.exit()
        if args.outputdirectory is None:
            args.outputdirectory = os.getcwd()
    else:
        if args.outputdirectory is None:
            args.outputdirectory = args.directory

        if len(glob.glob(os.path.join(args.directory,"*.bag"))) != 1:
            print(len(glob.glob(os.path.join(args.directory,"*.bag"))))
            print("no rosbag or more than 1")
            sys.exit()
        else:
            args.rosbag = glob.glob(os.path.join(args.directory,"*.bag"))[0]

        #if len(glob.glob(os.path.join(args.directory,"*_merged_log_v2.csv"))) != 1:
        if len(glob.glob(os.path.join(args.directory,"*_merged_log.csv"))) != 1:
            print("no poi data or more than 1")
            sys.exit()
        else:

            args.poi = glob.glob(os.path.join(args.directory,"*_merged_log.csv"))[0]

        if len(glob.glob(os.path.join(args.directory,"syncpoint"))) != 1:
            print("no syncpoint or more than 1")
            sys.exit()
        else:
            args.sync_videotime = glob.glob(os.path.join(args.directory,"syncpoint"))[0]

    if args.name is None:
        args.name = args.poi.split("_merged_log_v2.csv")[0]

    args.sync_videotime = float(np.loadtxt(args.sync_videotime))

    import multiprocessing
    q = multiprocessing.Queue()
    q.put(args)
    p = multiprocessing.Process(target = main, args = (q,))
    p.start()
    p.join()
    SAVE_FRAMES_TO_LABEL, SAVE_FRAMES_TO_SYNC, SAVE_FRAMES_TO_RAW = q.get()

    #path_ = os.path.join(args.outputdirectory,args.name + "_raw_frames_full.npy")
    #np.save(path,np.expand_dims(np.load(path),axis = 1))

    if args.visualfeedback:
        print("saving videos")
        print('ffmpeg -y -framerate 10 -i'.split() + [SAVE_FRAMES_TO_SYNC] + '/%5d.png -vf scale=776:-2'.split() + [os.path.join(args.outputdirectory,args.name+'_syncvid.mp4')])
        subprocess.call('ffmpeg -y -framerate 10 -i'.split() + [os.path.join(SAVE_FRAMES_TO_SYNC, '%5d.png')] + '-vf scale=776:-2'.split() + [os.path.join(args.outputdirectory,args.name+'_syncvid.mp4')])
        subprocess.call('ffmpeg -y -framerate 10 -i'.split() + [os.path.join(SAVE_FRAMES_TO_RAW, '%5d.png')] + '-vf scale=776:-2'.split() + [os.path.join(args.outputdirectory,args.name+'_rawvid.mp4')])
        subprocess.call('ffmpeg -y -framerate 10 -i'.split() + [os.path.join(SAVE_FRAMES_TO_LABEL,'%5d.png')] + '-vf scale=776:-2'.split() + [os.path.join(args.outputdirectory,args.name+'_labelvid.mp4')])
        # os.system('ffmpeg -y -framerate 40 -i ' + SAVE_FRAMES_TO_SYNC + '/%5d.png -vf scale=776:-2 ' + )
        # os.system('ffmpeg -y -framerate 40 -i ' + SAVE_FRAMES_TO_RAW + '/%5d.png -vf scale=776:-2 '+ os.path.join(args.outputdirectory,args.name+'_rawvid.mp4'))
        # os.system('ffmpeg -y -framerate 40 -i ' + SAVE_FRAMES_TO_LABEL + '/%5d.png -vf scale=776:-2 '+ os.path.join(args.outputdirectory,args.name+'_labelvid.mp4'))
        if not args.nocleanup:
            shutil.rmtree(SAVE_FRAMES_TO_SYNC)
            shutil.rmtree(SAVE_FRAMES_TO_RAW)
            shutil.rmtree(SAVE_FRAMES_TO_LABEL)
