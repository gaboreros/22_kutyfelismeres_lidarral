import os
import numpy as np
import subprocess
import pickle
from tempfile import gettempdir
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from detector_lib import get_legs, get_pixel_in_matrix, get_legs_multi

print("reading laser")
laser = np.load("/home/fbence/mounted/other/LUSTRE/HOME/dimia_ule_5/dimia_ule_5_1/DOGLEG/red_neuronal_bence_GPU/input/nicc_20190323-071603_raw_frames_full.npy")
print("reading result")
result = np.load("/home/fbence/mounted/other/LUSTRE/HOME/dimia_ule_5/dimia_ule_5_1/DOGLEG/red_neuronal_bence_GPU/output/full_points_run_model_dogleg.npy")
print("reading poi")
poi = pickle.load(open("/home/fbence/mounted/other/LUSTRE/HOME/dimia_ule_5/dimia_ule_5_1/DOGLEG/red_neuronal_bence_GPU/input/nicc_20190323-071603_poi_frames_full.pkl","rb"),encoding = "bytes")

SAVE_FRAMES = os.path.join(gettempdir(), 'legs_offline_{}'.format(hash(os.times())))
if os.path.isdir(SAVE_FRAMES):
    shutil.rmtree(SAVE_FRAMES)
os.mkdir(SAVE_FRAMES)
size = len(result)
counter = 0
i_ = 0
for i,(im,poi,l) in enumerate(zip(result,poi,laser)):
    counter += 1
    if counter == 100:
        print("{} / {}".format(i+1,size))
        counter = 0
    legs,legs_im = get_legs_multi(im)
    if divmod(i,4)[1] == 0:
        i_ += 1
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)
        ax.set_xlim([0,256])
        ax.set_ylim([0,256])
        ax.set_aspect('equal')
        xcoords = []
        ycoords = []
        for x in range(l.shape[1]):
            for y in range(l.shape[2]):
                if l[0][x][y] == 1:
                    xcoords.append(x)
                    ycoords.append(y)
        plt.scatter(xcoords,ycoords,s=3,c = "blue")
        for leg in legs_im:

            color = "red" if leg[3] == 1 else "orange"
            plt.scatter(leg[0],leg[1],s = 20,c = color)
        for p in poi:
            color = "red" if p[2] == 1 else "orange"
            x,y = get_pixel_in_matrix(p[0],p[1])
            poicircle = patches.Circle([x,y],radius=0.6*250/5,fill=True,alpha = 0.2, color = color)
            ax.add_patch(poicircle)
        plt.savefig(os.path.join(SAVE_FRAMES,"{:05d}.png".format(i_)))
        plt.close('all')


print(' '.join('ffmpeg -y -framerate 10 -i'.split() + [os.path.join(SAVE_FRAMES, '%5d.png')] + '-vf scale=776:-2'.split() + ['resvid_syncvid.mp4']))
subprocess.call('ffmpeg -y -framerate 10 -i'.split() + [os.path.join(SAVE_FRAMES, '%5d.png')] + '-vf scale=776:-2'.split() + ['resvid_syncvid.mp4'])
