# -*- coding: utf-8 -*-

'''
This if for stuff that works in both python3 and python2
'''
from __future__ import print_function,division,with_statement

import numpy as np
import cv2

import time

L = 256

'''
This essentially hardcodes the pixel to meter ratio to 250 pixels = 5 m.
'''
def get_pixel_in_matrix(x,y):
    j = 0
    k = 0
    half_matrix = L / 2.

    point_x = int (np.round((x * 100)/2.))
    point_y = int (np.round((y * 100)/2.))

    j = point_x
    k = half_matrix - point_y

    return j,int(k)

def get_legs(image):
    count_legs = 0
    list_legs = []
    list_legs_im = []
    #cv2.imwrite('/home/claudia/Escritorio/imagen1.png',image)
    # Dilatamos los puntos blancos que nos devulve la red para hacerlos más significativos.
    kernel = np.ones((2,2),np.uint8)
    image = cv2.dilate(image,kernel,iterations = 1)

    # Calculamos el threshold entre [100 y 255] de la imagen y lo escalamos

    _, thresh = cv2.threshold(image,0.5,255,cv2.THRESH_BINARY)

    thresh = cv2.convertScaleAbs((255*thresh/np.max(thresh)))

    # Buscamos los contornos de la imagen y recogemos los momentos
    #_, contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #print(len(contours))
    for i in range(0, len(contours)):
        (x,y),radius = cv2.minEnclosingCircle(contours[i])
        center = (int(x),int(y))
        radius = int(radius)
        #cv2.circle(image,center,radius,(255,255,255),2)
        #cv2.imwrite('/home/claudia/Escritorio/imagen2.png',image)

        x_frame, y_frame = get_pixel_in_matrix(x, y)
        # Almacenamos los datos de la pierna en un PositionMeasurement

        # Almacenamos la informacion en una lista de piernas
        list_legs.append([x_frame,y_frame])
        list_legs_im.append([x,y,radius])
        count_legs = count_legs + 1
    return list_legs,list_legs_im


def get_legs_multi(image):
    count_legs = 0
    list_legs = []
    list_legs_im = []
    #cv2.imwrite('/home/claudia/Escritorio/imagen1.png',image)
    # Dilatamos los puntos blancos que nos devulve la red para hacerlos más significativos.
    kernel = np.ones((2,2),np.uint8)
    image = cv2.dilate(image,kernel,iterations = 1)

    # Calculamos el threshold entre [100 y 255] de la imagen y lo escalamos

    _, thresh1 = cv2.threshold(image,0.75,255,cv2.THRESH_BINARY)
    _, thresh2 = cv2.threshold(image,0.25,255,cv2.THRESH_BINARY)
    thresh2 = thresh2 - thresh1
    thresh1 = cv2.convertScaleAbs((255*thresh1/np.max(thresh1)))
    thresh2 = cv2.convertScaleAbs((255*thresh2/np.max(thresh2)))

    # Buscamos los contornos de la imagen y recogemos los momentos
    #_, contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours1, _ = cv2.findContours(thresh1, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    contours2, _ = cv2.findContours(thresh2, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #print(len(contours))
    for i in range(0, len(contours1)):
        (x,y),radius = cv2.minEnclosingCircle(contours1[i])
        center = (int(x),int(y))
        radius = int(radius)
        #cv2.circle(image,center,radius,(255,255,255),2)
        #cv2.imwrite('/home/claudia/Escritorio/imagen2.png',image)

        x_frame, y_frame = get_pixel_in_matrix(x, y)
        # Almacenamos los datos de la pierna en un PositionMeasurement

        # Almacenamos la informacion en una lista de piernas
        list_legs.append([x_frame,y_frame,1])
        list_legs_im.append([x,y,radius,1])
        count_legs = count_legs + 1
    for i in range(0, len(contours2)):
        (x,y),radius = cv2.minEnclosingCircle(contours2[i])
        center = (int(x),int(y))
        radius = int(radius)
        #cv2.circle(image,center,radius,(255,255,255),2)
        #cv2.imwrite('/home/claudia/Escritorio/imagen2.png',image)

        x_frame, y_frame = get_pixel_in_matrix(x, y)
        # Almacenamos los datos de la pierna en un PositionMeasurement

        # Almacenamos la informacion en una lista de piernas
        list_legs.append([x_frame,y_frame,2])
        list_legs_im.append([x,y,radius,2])
        count_legs = count_legs + 1
    return list_legs,list_legs_im


# Método que pasa el punto recibido en pixel sobre la imagen a un punto (x,y) dentro del frame hukuyo_laser_link
