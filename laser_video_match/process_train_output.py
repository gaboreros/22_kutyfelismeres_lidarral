import os
import cv2
import numpy as np
import pickle
from tempfile import gettempdir
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as patches

from detector_lib import get_legs, get_pixel_in_matrix, get_legs_multi

data = np.load("/home/fbence/mounted/other/LUSTRE/HOME/dimia_ule_5/dimia_ule_5_1/DOGLEG/red_neuronal_bence_GPU/output/test_labels_train1_multiGP_dogleg.npy")
# np.save("testdata",data[:100])

poi = pickle.load(open("/home/fbence/mounted/other/LUSTRE/HOME/dimia_ule_5/dimia_ule_5_1/DOGLEG/red_neuronal_bence_GPU/output/test_pois_train1_multiGP_dogleg.pkl","rb"),encoding = "bytes")
SAVE_FRAMES = os.path.join(gettempdir(), 'legs_{}'.format(hash(os.times())))
if os.path.isdir(SAVE_FRAMES):
    shutil.rmtree(SAVE_FRAMES)
os.mkdir(SAVE_FRAMES)

# for i,(im,poi) in enumerate(zip(data[:20],poi[:20])):
#     legs,legs_im = get_legs(im)
#     # print("legs",legs)
#     # print("legs_im",legs_im)
#     # print("poi",poi)
#     for leg in legs_im:
#         center = (int(leg[0]),int(leg[1]))
#         radius = int(leg[2])
#
#         cv2.circle(im,center,radius,(255,255,255),2)
#     for p in poi:
#         x,y = get_pixel_in_matrix(p[0],p[1])
#         cv2.circle(im,(int(x),int(y)),20,(255,255,255),1)
#     cv2.imwrite(os.path.join(SAVE_FRAMES,"{:05d}.png".format(i)),im)


for i,(im,poi) in enumerate(zip(data[:20],poi[:20])):
    legs,legs_im = get_legs_multi(im)
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
    ax.set_xlim([0,256])
    ax.set_ylim([0,256])
    ax.set_aspect('equal')
    for leg in legs_im:

        color = "red" if leg[3] == 1 else "orange"
        plt.scatter(leg[0],leg[1],s = 20,c = color)
    for p in poi:
        color = "red" if p[2] == 1 else "orange"
        x,y = get_pixel_in_matrix(p[0],p[1])
        poicircle = patches.Circle([x,y],radius=0.6*250/5,fill=True,alpha = 0.2, color = color)
        ax.add_patch(poicircle)
    plt.savefig(os.path.join(SAVE_FRAMES,"{:05d}.png".format(i)))
