# Leírás

Ez a környzete arra szolgál, hogy a laborban történt méréseket fel tudjuk dolgozni, és a kívánt munkálatokat végre tudjuk hajtani rajta.

# Beállítás

A munkálatokat `python 3.7.6` verzióval hajtottuk végre, de valószínűleg a frisebb verziókkal is működni fog.
A környezet létrehozására az alábbi teendők szükségesek:

1. Telepítsd a szükséges package-ket az alábbi fájlból: requirements.txt (`pip install -r requirements.txt`).

2. Telepítsd: tensorflow 1.13.1:

 - nvidia gpu: `pip install tensorflow-gpu==1.13.1`
 - no nvidia gpu: `pip install tensorflow==1.13.1`

 Hogyha van GPU-d, akkor szükséges a CUDA telepítése, de nagyon fontos a PONTOS verziók egyeztetése!
  `tensorflow 1.13.1` esetünkben `CUDA 10.0`, `cudnn 7.3` - al működik, úgyhogy ezeket a speciális verziókat szükséges telepíteni.
  Itt megtalálsz minden drivert:
  [This](https://developer.nvidia.com/cuda-10.0-download-archive)
 Továbbá fontos, hogy a `cudnn` legyen a PATH-ban
 GPU nélkül is használhatod, de nagyon lassú lesz!
 
 Megjegyzés:
 	probléma léphet fel `pycocotools==2.0.0` - telepítésével, ebben az esetben az alábbi módon telepíthetjük:
	-telepíteni visual sudio build tools-t [This](https://landinghub.visualstudio.com/visual-cpp-build-tools)
	-`pip install git+https://github.com/philferriere/cocoapi.git#subdirectory=PythonAPI`

3. Klónozd `Mask_RCNN` az alábbi repoból:
```
git clone git@bitbucket.org:elteethologydepartment/mask_rcnn.git Mask_RCNN
```

vagy

```
https://github.com/matterport/Mask_RCNN
```

Ellenőrizni szükséges, hogy benne van a`PYTHONPATH`-ban:

Linux operációs rendszer esetén:`.bashrc`
```
export PYTHONPATH="$PYTHONPATH:/home/fbence/Codes"
```

Windows esetében: [here](https://stackoverflow.com/questions/3701646/how-to-add-to-the-pythonpath-in-windows).

4. Telepítsd `ffmpeg`.
See [here](https://github.com/adaptlearning/adapt_authoring/wiki/Installing-FFmpeg).

5. A környezet elleőrzésére: `python test_installation.py`.

# Mask RCNN

Előre tanított neurális hálókat használ, hogy objektumokat találjanak meg képeken/ videókon.
Futtasd az alábbi kódok, hogy megértsd, hogy hogy is működik (a program egy bounding box-ot, és egy felismerési valószínűséget fog visszaadni):
```
from Mask_RCNN import default_model, default_class_names, visualize
import skimage.io
image = skimage.io.imread("images/andersen.jpg",plugin="matplotlib")
r = default_model.detect([image],verbose=1)[0]
visualize.display_instances(image,r['rois'],r['masks'],r['class_ids'],default_class_names,r['scores'])
```
![Instance Segmentation Sample](images/andersen_output.png)

A demo esetén lehet probléma a matplotlib-el. Ennek megoldása:
import matplotlib.pyplot as plt
matplotlib.use( 'tkagg' )

# Programok futtatási sorrendje:

0. Legyen az összes felvett videó egy mappában.

1. Ellenőrizd, hogy az aruco template set megfelel-e a videóidnak.

2. Futtasd: process_videos.py a mappán.

3. Futtasd: process_logs.py a mappán.

4. Futtasd: merge_logs.py a mappán.

Vagy használd: `chain.py` az összefűzött futtatásért.

## Chain.py

Egy gyorstesztet csinál az arucora és 90% felett elfogadja, de lehet erőltetve futtatni `-f`.
```
usage: chain.py [-h] -d DIR [-a DIR] [--num_cameras NUM_CAMERAS]
                [--num_frames NUM_FRAMES] [--camfps CAMFPS] [-f]
                [--makevideos] [--recalculate]

Process the logs output from process videos.

optional arguments:
  -h, --help            show this help message and exit
  -d DIR, --directory DIR
                        directory with input videos
  -a DIR, --arucopath DIR
                        directory with aruco stuff
  --num_cameras NUM_CAMERAS
                        number of cameras
  --num_frames NUM_FRAMES
                        number of frames to process
  --camfps CAMFPS       fps of cameras
  -f, --force           force even if automatic aruco test fails
  --makevideos          create feedbackvideos
  --recalculate         dont skip already finished files but calculate again
```

## Aruco ellenőrzés

Használd: `camangle_calibration.py`.

```
python camangle_calibration.py -h

usage: camangle_calibration.py [-h] -d DIR [-o DIR] [-a DIR]
                               [--num_cameras NUM_CAMERAS] [--camfps CAMFPS]
                               (--quicktest | --test | --get_all_frames | --generate_aruco | --get_num_frames GET_NUM_FRAMES)

Check camera setup.

optional arguments:
  -h, --help            show this help message and exit
  -d DIR, --directory DIR
                        directory with input videos
  -o DIR, --outputdir DIR
                        output directory (default is input dir)
  -a DIR, --arucopath DIR
                        directory with aruco stuff
  --num_cameras NUM_CAMERAS
                        number of cameras
  --camfps CAMFPS       fps of cameras
  --quicktest           check first video frames
  --test                supply your own frames
  --get_all_frames      unpacks the videos
  --generate_aruco      generate new aruco setup
  --get_num_frames GET_NUM_FRAMES
                        number of frames to get

```

### `--quicktest`

- kiveszi az első frameket a videókból és megpróbálja összefűzni őket
- meg kell adni a bemeneti mappát: (`-d`) és az elérési út
- egy mappát fog generálni: `frames` a `-d`-ben, majd beleteszi a frameket és az aruco matcheket
- valami ilyesmit fog generálni:
```
1 0.9000402 cam1.png
5 0.9081019 cam5.png
3 0.9257446 cam3.png
2 0.912317 cam2.png
4 0.9015441 cam4.png
```

Használat:

```
python camangle_calibration.py -d /path/to/videos --quicktest -a aruco_templates/2019_04_17/
```

### Aruco setup létrehozása

```
python camangle_calibration.py -d /path/to/your/folder --generate_aruco
```

#### Aruco setup megértése
Az `aruco_locations.csv` tartja a padló feltérképezését.
A táblázat felépítése:

| camera_id | lab_x | lab_y |pic_x |pic_y |
|-|-|-|-|-|
| 1 | 2 | 3 | 52 | 52 |


Egy `opencv`-t használunk: [findHomography](https://www.learnopencv.com/homography-examples-using-opencv-python-c/) hogy feltérképezze a pixel koordinátákat a labor koordinátákra.

## Process video
```
usage: process_videos.py [-h] -d DIR [-o DIR] [-a DIR]
                         [--num_frames NUM_FRAMES] [--num_cameras NUM_CAMERAS]
                         [--camfps CAMFPS] [--nocleanup] [--makevideos]
                         [--recalculate]

Process the videos.

optional arguments:
  -h, --help            show this help message and exit
  -d DIR, --directory DIR
                        directory with input videos
  -o DIR, --outputdir DIR
                        output directory (default is input dir)
  -a DIR, --arucopath DIR
                        directory with aruco stuff
  --num_frames NUM_FRAMES
                        number of frames to process
  --num_cameras NUM_CAMERAS
                        number of cameras
  --camfps CAMFPS       fps of cameras
  --nocleanup           leave frames in tempdir
  --makevideos          create some feedbackvideos
  --recalculate         dont skip already finished files but calculate again

```

Ez minden talált videóra egy log file-t készít. A neve ugyanaz lesz, mint az eredeti videónak`_raw_log.csv` végződéssel. Hogyha `--makevideos` engedélyezve van, akkor minden talált videóra a maszkkal és a felismerési valószínűséggel egy videót fog generálni.

A fájlban az első sorban:

- kamera azonosító
- `pyssim` felismerési index
- az eredeti aruco mappa, amelyet használja

Minden sorhoz az alábbiakat rendeli hozzá (minden koordináta pixel koordinátában:

- a frame id amelyiken megtalálta a Mask_RCNN
- az `x` és `y` koordinátája a bounding box közepének
- az `x` és `y` koordinátája a bounding box bal alsó pontnak
- az `x` és `y` koordinátája a bounding box jobb felső pontnak


## Process logs

```
usage: process_logs.py [-h] -d DIR [-o DIR] [-a DIR]
                       [--num_cameras NUM_CAMERAS]

Process the logs output from process videos.

optional arguments:
  -h, --help            show this help message and exit
  -d DIR, --directory DIR
                        directory with input videos
  -o DIR, --outputdir DIR
                        output directory (default is input dir)
  -a DIR, --arucopath DIR
                        directory with aruco stuff
  --num_cameras NUM_CAMERAS
                        number of cameras
```

Ez minden talált `_raw_log.csv`-ra a következőket generálja le:
- `_processed_log_block.csv`
- `_processed_log_second.csv`
- `_warped_log_block.csv`
- `_warped_log_second.csv`

Először processed logokban, pixel koordinátákat helyezi, interpolációt és átlagolásokat használ, hogy folytonosabbá tegye.

Majd mindegyikre egy warped fájt csinál, melyben a pixel koordinátákat fizikai koordinátákra generálja. A transzformáció a bounding box aljának a közepét veszi alapul.

A `second` esetén az első oszlopban nem frame-k, hanem másodpercben vannak az adatok.

## merge logs

```
usage: merge_logs.py [-h] -d DIR [-o DIR] [--num_cameras NUM_CAMERAS]
                     [--camfps CAMFPS] [--nocleanup] [--makevideos]

Merge logs output from process logs.

optional arguments:
  -h, --help            show this help message and exit
  -d DIR, --directory DIR
                        directory with input videos
  -o DIR, --outputdir DIR
                        output directory (default is input dir)
  --num_cameras NUM_CAMERAS
                        number of cameras
  --camfps CAMFPS       fps of cameras
  --nocleanup           leave frames in tempdir
  --makevideos          create some feedbackvideos
```

Veszi a `_warped_log.csv`-ket és összefűzi őket `_merged_log.csv`-ba. Ez egyszerű átlagolással történik.
A kimenetek: `frame id`, `x` és `y` fizikai koordináták.

Ha `--makevideos` engedélyezve van, videót is generál, ahogy az adatok és a gondolt kutya mozog.

## A koordináta rendszerek

A labor 540 cm X 627 cm-s. Az aruco tag koordináta rendszer a távolgásokat méterben, amíg a legtöbb kimenet cm-t alkalmaz.
A `camangle_calibration` ilyen képeket alkalmaz. A színezett terültetek, amelyeket a kamerák látnak.

![](aruco_templates/2019_04_17/views/lab_full_2019_apr_13.jpg)

### Példa projekt

```
python chain.py -d /path/to/mytest -f --makevideos --num_frames 300
```

# Laser_video_match

`laser_video_match.py` : összekapcsolha a rosbag-ket a videó követéssel

`train.py` : a tanítás elvégzése, a képeket min numpy array-ket kell megadni


