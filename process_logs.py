import numpy as np
import sys
import cv2
import os
import glob
import argparse
import logging
logger = logging.getLogger("process_logs")

from camangle_calibration import get_homographies,warp_coordinates

def check_data_validity(data):
    prevframe = -1
    for d in data:
        if prevframe >= d[0]:
            logger.error("frames not consecutive")
            return False
        prevframe = d[0]
    return True

def get_log_data(log_path):
    data = []
    firstline = True
    with open(log_path, 'r') as logfile:
        for line in logfile.readlines():
            if firstline:
                camid = int(line.split()[1])
                acc = float(line.split()[2])
                origpath = line.split()[3]
                firstline = False
            else:
                split_line = line.split(',')
                # Is the data processed already?
                bound = len(split_line) - 1
                for i in range(bound):
                    split_line[i] = int(split_line[i])

                split_line[-1] = split_line[-1].split('\n')[0]
                data.append(split_line)
    #print(data)
    #print("acc: ")
    #print(acc) #--hányas kamera, acc-similarity valami valószínűség
    #print("BEOLVASVA")
    return data,camid,acc,origpath

def select_classes(log, class_list): #visszaadja a kívánt classokat tartalmazó frameket, label nélkül
    #log-adat, class_list-miket olvas
    '''
    Select entries of our log which contain information about
    classes of interest. After deleting the irrelevant entries,
    remove the class label as well, in order to make our log
    consist only of integer data.
    '''
    new_log = []
    for log_element in log:
        for class_element in class_list:
            if class_element == log_element[-1]:
                new_log.append(log_element[:-1])
                break
    #print(new_log)
    return new_log

def miss_interpolation(log, threshold, verbose=True):
    #threshold=60, nem is használja NEM
    '''
    We assume that collisions have been eliminated before running interpolation.
    Linearly interpolate missing data. If the distance between the two non-continous
    frames is greater than our threshold, assume that the instance is missing in that interval.
    '''
    # We want to copy the original log
    new_log = log[:]
    interpolation_values = []
    for i in range(len(new_log) - 1, 0, -1):
        to_append = []
        # Framedifference is greater than our interpolation threshold
        frame_difference = new_log[i][0] - new_log[i-1][0]
        if frame_difference != 1 and frame_difference < threshold:
            if verbose:
                print('MISSING DATA: {0}-{1}, LENGTH: {2}'.format(new_log[i][0], new_log[i-1][0], frame_difference))
            for j in range(len(new_log[i])):
                to_append.append(np.linspace(new_log[i-1][j], new_log[i][j], frame_difference + 1)[1:-1].astype(int).tolist())
            interpolation_values = interpolation_values + np.transpose(to_append).tolist()
    new_log = new_log + interpolation_values
    new_log.sort(key = lambda x: x[0])
    return new_log

def eliminate_collisions(log, verbose=True):
    #hogyha van egyezés, akkor a nagyobb távolságút leveszi
    '''
    Need a better solution for this if we want to track multiple instances of the same class.
    Currently its sole purpose is to differentiate 'true' dog-cats.
    '''
    # We want to copy the original log
    new_log = log[:]
    for i in range(len(new_log) - 1, 0, -1):
        if new_log[i][0] == new_log[i-1][0]:
            # Edge cases, or previous frame missing, varying relative vector
            if i == 1 or new_log[i-2][0] - new_log[i][0] > 1:
                relative_i = i+1
            else:
                relative_i = i-2
            i_vector = np.array([new_log[i][2], new_log[i][1]])
            iminus_vector = np.array([new_log[i-1][2], new_log[i-1][1]])
            relative_vector = np.array([new_log[relative_i][2], new_log[relative_i][1]])
            dist_i = np.linalg.norm(i_vector - relative_vector)
            dist_iminus = np.linalg.norm(iminus_vector - relative_vector)
            if verbose:
                print('Collision found at frame {0}:'.format(new_log[i][0]))
                print('dist(i, i-2): {0}'.format(dist_i))
                print('dist(i-1, i-2): {0}'.format(dist_iminus))
            if dist_i < dist_iminus:
                new_log.pop(i-1)
            else:
                new_log.pop(i)
            #print('ez lefutott') 1-2-es warpedre meghívódik
    return new_log

def decompose_log(log, threshold):
    #blokkokra bontja szét az adathalmazt
    '''
    Decompose the log to connected blocks
    to precisely run MA. Assumes that we already
    ran interpolation and eliminated collisions!
    Threshold should equal to the interpolation threshold.
    '''
    log_blocks = []
    
    # Where is the end of the latest block
    bound = 0
    for i in range(1, len(log)):
        if log[i][0] - log[i-1][0] > threshold:
            #print(log[i][0])
            log_blocks.append(log[bound:i])
            bound = i
           
    # Add the last blocksegment
    log_blocks.append(log[bound:])
    return log_blocks

def patient_moving_average(block, n=5):
    
    '''
        Leaves elements at the bounds as they are, to preserve dimensions.
        Our log entries are locally very similar (but not smoothly similar),
        so this isn't a problem.
        A moving average will not infer with the frames,
        as we have already done interpolation, and the
        blocksegmentation threshold is the same.
    '''
    block_transpose = np.transpose(block)
    if len(block) < n:
        return block
    new_block = []
    for i in range(len(block_transpose)):
        to_add = np.concatenate((block_transpose[i][:n-n//2 - 1], np.convolve(block_transpose[i], np.ones(n), mode='valid')//n, block_transpose[i][-n+n//2 + 1:]))
        new_block.append(to_add.astype('int'))
    return np.transpose(new_block)

def block_moving_average(log, n=5):
    '''
    Decompose the frames into blocks, each block containing
    a continous frame-sequence where the instance is visible.
    '''
    # TO DO: PRECISE ERROR MANAGEMENT
    if n % 2 != 1 or type(n) != int:
        raise RuntimeError('Invalid value for \'n\'')
    log_blocks = decompose_log(log, n)
    #print(log_blocks)
    new_block = []
    for i in range(len(log_blocks)):
        new_block.append(patient_moving_average(log_blocks[i], n))
    #print(np.concatenate((tuple(new_block))))
    return np.concatenate((tuple(new_block)))

def second_average(log, threshold = 15):
    #nem működik rendesen
    '''
    Apply to processed log only. Takes a second-wise average on
    frames where the instance is visible.
    '''
    # Integer division by 30 on the first element to get which second are we on

    if len(log) < 1:
        return []
    second_log = [np.concatenate(([log_element[0] // 30],log_element[1:])) for log_element in log]
    sec_logs = [] # Will be the data averaged over seconds
    bound = 0
    cur_second = second_log[0][0]
    for i in range(1, len(second_log)):
        if second_log[i][0] != cur_second:
            if i - bound > threshold:
                cur_second = second_log[i][0]
                temp_array = second_log[bound]
                # Will not be out of bounds because of set threshold
                for j in range(bound + 1, i):
                    temp_array = np.add(temp_array, second_log[j])
                temp_array = temp_array //(i-bound)
                sec_logs.append(temp_array)
                bound = i
            else:
                cur_second = second_log[i][0]
                bound = i

    if len(second_log) - bound > threshold:
        temp_array = second_log[bound]
        for i in range(bound + 1, len(second_log)):
            temp_array = np.add(temp_array, second_log[i])
        temp_array = temp_array //(len(second_log) - bound)
        sec_logs.append(temp_array)
    return sec_logs

def write_processed_log(log, dest, keep_boundingbox_info=False):
    with open(dest, 'w') as output:
        for log_element in log:
            # Convert the log_elements to a comma-delimited string
            if keep_boundingbox_info:
                output.write(','.join(str(element) for element in log_element[:3]) + '\n')
            else:
                output.write(','.join(str(element) for element in log_element) + '\n')



#def compile_single_line_format(data):
 #   end
def process_log(LOG_PATH,outdir,arucopath,verbose=True,num_humans=1,num_dogs=1):
#def process_log(LOG_PATH,outdir,arucopath,verbose = True, num_humans = 1, num_dogs = 1):
    basename = os.path.splitext(os.path.basename(LOG_PATH))[0].split("_raw_log")[0]
    LOG_OUTPUT_PATH = os.path.join(outdir,basename + "_processed_log_block.csv")
    SECOND_LOG_OUTPUT_PATH =  os.path.join(outdir,basename + "_processed_log_second.csv")
    WARPED_LOG_OUTPUT_PATH =  os.path.join(outdir,basename + "_warped_log_block.csv")
    WARPED_SEC_LOG_OUTPUT_PATH = os.path.join(outdir,basename + "_warped_log_second.csv")
    
    
    default_classes = ['dog', 'cat']

    data,camid,acc,origpath = get_log_data(LOG_PATH)
    if acc < 0.8:
        logger.warning("Warning: low similarity with original aruco: {}".format(acc))
    if origpath != arucopath:
        logger.warning("Warning: aruco paths dont match")
    aruco_data = np.loadtxt(os.path.join(arucopath,"aruco_locations.csv"),delimiter=',').astype(int)
    data = select_classes(data, default_classes)

    data = eliminate_collisions(data,verbose)

    data = miss_interpolation(data, 60,verbose)
    check_data_validity(data)
    data = block_moving_average(data, 9)
    check_data_validity(data)
    data_seconds = second_average(data)

    logger.info('Finished processing {0}'.format(LOG_PATH))

    homographies = get_homographies(aruco_data)
    if len(data) > 0:
        warped_coordinates = warp_coordinates([[data_entry[2], data_entry[1]] for data_entry in data], homographies, camid)
        warped_coordinates_bottom_left = warp_coordinates([[data_entry[5], data_entry[4]] for data_entry in data], homographies, camid)
        warped_coordinates_bottom_right = warp_coordinates([[data_entry[6], data_entry[4]] for data_entry in data], homographies, camid)

    if len(data) > 0:
        warped_sec_coordinates = warp_coordinates([[data_entry[2], data_entry[1]] for data_entry in data_seconds], homographies, camid)
        warped_sec_coordinates_bottom_left = warp_coordinates([[data_entry[5], data_entry[4]] for data_entry in data_seconds], homographies, camid)
        warped_sec_coordinates_bottom_right = warp_coordinates([[data_entry[6], data_entry[4]] for data_entry in data_seconds], homographies, camid)

    final_warped_coords = []
    final_warped_sec_coords = []

    # Write warped framedata to file
    for i in range(len(data)):
        # FRAME - DOG HEIGHT IN CM - DOG WIDTH IN CM
        final_warped_coords.append([data[i][0], int(warped_coordinates[i][0]*100), int(warped_coordinates[i][1]*100),int(warped_coordinates_bottom_left[i][0]*100), int(warped_coordinates_bottom_left[i][1]*100),int(warped_coordinates_bottom_right[i][0]*100), int(warped_coordinates_bottom_right[i][1]*100)])

    # Write warped second data to file
    for i in range(len(data_seconds)):
        # SECOND - DOG HEIGHT IN CM - DOG WIDTH IN CM
        final_warped_sec_coords.append([data_seconds[i][0], int(warped_sec_coordinates[i][0]*100), int(warped_sec_coordinates[i][1]*100),int(warped_sec_coordinates_bottom_left[i][0]*100), int(warped_sec_coordinates_bottom_left[i][1]*100),int(warped_sec_coordinates_bottom_right[i][0]*100), int(warped_sec_coordinates_bottom_right[i][1]*100)])

    logger.info('Finished processing coordinate warping!')

    write_processed_log(data, LOG_OUTPUT_PATH)
    write_processed_log(data_seconds, SECOND_LOG_OUTPUT_PATH)
    write_processed_log(final_warped_coords, WARPED_LOG_OUTPUT_PATH)
    write_processed_log(final_warped_sec_coords, WARPED_SEC_LOG_OUTPUT_PATH)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process the logs output from process videos.")
    parser.add_argument("-d", "--directory",
                help="directory with input videos", metavar="DIR",
                type=str,required = True)
    parser.add_argument("-o", "--outputdir",
                help="output directory (default is input dir)", metavar="DIR",
                type=str)

    parser.add_argument("-a", "--arucopath",
                help="directory with aruco stuff", metavar="DIR",default = os.path.join(os.path.dirname(os.path.realpath(__file__)),"aruco_templates","2019_03_23"),
                type=str)
    parser.add_argument("--num_cameras",
                help="number of cameras", default = 5,
                type=int)

    parser.add_argument("-nd","--num_dogs",
                help="number of dogs in scene", default = 1,
                type=int)
    parser.add_argument("-nh","--num_humans",
                help="number of humans in scene", default = 1,
                type=int)
    parser.add_argument('--verbose',help="verbosity", action='store_true')

    args = parser.parse_args()


    if args.outputdir is None:
        args.outputdir = args.directory

    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler(os.path.join(args.outputdir,"biglab_videorecognition.log"))
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    for log in glob.glob(os.path.join(args.directory,"*_raw_log.csv")):
        logger.info(log)
        process_log(log,args.outputdir,args.arucopath,verbose = args.verbose, num_humans = args.num_humans, num_dogs = args.num_dogs)

def compile_single_line_format(data):
    print("vege")